/*
* Author: Kenny Van
* Username: kennyv
* Lab #: Lab5
* Lab Section: 001
* Submitted on: 2/15/19
* Name of TA: Nushrat Humaira
* Academic Honesty Declaration: 
* The following code represents my own work and I have neither received nor given assistance that violates the collaboration policy posted with this assignment. I have not copied or modified code from any other source other than the lab assignment, course textbook, or course lecture slides. Any unauthorized collaboration or use of materials not permitted will be subjected to academic integrity policies or Clemson University and CPSC 1010/1011. I acknowledge that this lab assignment is based upon an assignment created by Clemson University and that any publishing or posting of this code is prohibited unless I receive written permission from Clemson University.
*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
	//Used multiple for loops to initialize the deck and give the cards a suit simultaneously
	int i;
	Card deck[52];
	for(i=0;i<13;i++){
		deck[i].value = i+2;
		deck[i].suit=Suit::SPADES;
	}
	for(i=13;i<26;i++){
		deck[i].value = i-11;
		deck[i].suit=Suit::HEARTS;
	}
	for(i=26;i<39;i++){
		deck[i].value = i-24;
		deck[i].suit=Suit::DIAMONDS;
	}
	for(i=39;i<52;i++){
		deck[i].value = i-37;
		deck[i].suit=Suit::CLUBS;
	}
  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
	random_shuffle(deck, deck + 52, myrandom);

   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
	Card hand[6];
		//Uses a for loop to create a hand of five cards
		for(int i = 0; i < 6; i++){
			hand[i] = deck[i];
		}
	

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
	sort(hand, hand + 6, suit_order);

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
	for(int i = 0; i < 6; i++){
		cout << setw(10) << get_card_name(hand[i]);
		cout << get_suit_code(hand[i]) << endl;
	}

  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  // IMPLEMENT
	if(lhs.suit < rhs.suit){return true;}
	else if(lhs.suit == rhs.suit){
		if(lhs.value < rhs.value){return true;}
		else{return false;}
	}
	else{return false;}
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  switch(c.value){
	case 2: return "2 of ";
	case 3:	return "3 of ";
	case 4:	return "4 of ";
	case 5:	return "5 of ";
	case 6:	return "6 of ";
	case 7:	return "7 of ";
	case 8:	return "8 of ";
	case 9:	return "9 of ";
	case 10: return "10 of ";
	case 11: return "Jack of ";
	case 12: return "Queen of ";
	case 13: return "King of ";
	case 14: return "Ace of ";
	default: return "";
}
}
